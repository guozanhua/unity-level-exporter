using UnityEngine;
using UnityEditor;
using System.Collections;

public class PathNode : MonoBehaviour
{
	public PathNode next;
	public PathNode previous;
	public int ID = -1;
	
	void OnDrawGizmos()
	{
		Gizmos.DrawIcon(gameObject.transform.position, "path.png");
		if (next) {
			next.previous = this;
			
			// Colour to determine if its one-way or two-way
			Gizmos.color = new Color(1.0f,0.0f,0.0f,1.0f);
			
			Vector3 rot = Vector3.Cross(next.transform.position,gameObject.transform.position).normalized;
			Vector3 p1 = (gameObject.transform.position-next.transform.position).normalized;
			Vector3 p2 = (gameObject.transform.position-next.transform.position).normalized;
			p1 = Quaternion.AngleAxis(45, rot) * p1;
			p2 = Quaternion.AngleAxis(-45, rot) * p2;
			Gizmos.DrawLine(next.transform.position, next.transform.position+p1);
			Gizmos.DrawLine(next.transform.position, next.transform.position+p2);
			
			// Connection Line
			Gizmos.DrawLine(gameObject.transform.position, next.transform.position);
		}
	}
}

public class CreatePathNode : EditorWindow
{   
	static Camera cam;
	static int nodeID = 0;
	
	[MenuItem("Shuriken/Add Path Node", false, 2)]
    static void Init()
    {
		cam = Camera.current;
		
		GameObject pathNode = new GameObject();
		pathNode.AddComponent<PathNode>();
		if (cam) {
			pathNode.transform.position = cam.transform.position + cam.transform.forward*5.0f;
		}
		pathNode.tag = "PathNode";
		pathNode.name = "Path Node";
		pathNode.GetComponent<PathNode>().ID = nodeID++;
		Selection.activeObject = pathNode;
	}
}