// Material Script
// Overrides the default material defined in the exporter and allows the user to define their own material values.
// Currently supported values are limited to basic lighting info (ambient, diffuse, etc.)

[System.Serializable]
public class Ambient {
	public float R = 1.0f;
	public float G = 1.0f;
	public float B = 1.0f;
}

[System.Serializable]
public class Diffuse {
	public float R = 1.0f;
	public float G = 1.0f;
	public float B = 1.0f;
}

[System.Serializable]
public class Specularity {
	public float R = 0.2f;
	public float G = 0.2f;
	public float B = 0.2f;
	public float A = 1.0f;
	public float Shinyness = 25.0f;
}

public class MaterialScript : UnityEngine.MonoBehaviour {
	public Ambient ambient;
	public Diffuse diffuse;
	public Specularity specularity;
	public bool lighting = true;
}
